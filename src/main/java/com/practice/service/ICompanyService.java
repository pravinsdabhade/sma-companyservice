package com.practice.service;

import java.util.List;

import com.practice.entity.Company;

public interface ICompanyService {

	Long createCompany(Company com);

	void updateCompany(Company com);

	Company getOneCompany(Long id);

	List<Company> getAllCompanies();

}
