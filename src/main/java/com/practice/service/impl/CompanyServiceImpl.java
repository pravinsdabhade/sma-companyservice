package com.practice.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.practice.entity.Company;
import com.practice.exception.CompanyNotFoundException;
import com.practice.repo.CompanyRepository;
import com.practice.service.ICompanyService;

@Service
public class CompanyServiceImpl implements ICompanyService {

	@Autowired
	private CompanyRepository repo;

	@Override
	public Long createCompany(Company com) {
		return repo.save(com).getId();
	}

	@Override
	public void updateCompany(Company com) {
		if (com.getCregId() != null && repo.existsById(com.getId())) {
			repo.save(com);
		}
	}

	@Override
	public Company getOneCompany(Long id) {
		Optional<Company> opt = repo.findById(id);
		if (opt.isPresent()) {
			return opt.get();
		} else {
			throw new CompanyNotFoundException("Given '" + id + "' Not Exist");
		}
	}

	@Override
	public List<Company> getAllCompanies() {
		return repo.findAll();
	}

}
