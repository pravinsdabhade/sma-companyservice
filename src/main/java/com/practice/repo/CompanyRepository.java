package com.practice.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.practice.entity.Company;

public interface CompanyRepository extends JpaRepository<Company, Long> {

}
