package com.practice.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.practice.entity.Company;
import com.practice.exception.CompanyNotFoundException;
import com.practice.service.ICompanyService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/company")
public class CompanyRestController {

	@Autowired
	private ICompanyService service;

	// 1. Create company
	@PostMapping("/create")
	public ResponseEntity<String> createCompany(@RequestBody Company com) {
		log.info("Entered into save method");
		ResponseEntity<String> respose = null;
		try {
			Long id = service.createCompany(com);
			respose = ResponseEntity.ok("Created With ID : " + id);
			log.info("COMPANY is CREATED {}.", id);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		log.info("about to leave save method.");
		return respose;
	}

	// 2. Fetch all Companies
	@GetMapping("/all")
	public ResponseEntity<List<Company>> getAllComponies() {
		log.info("Entered into fetch all method.");
		ResponseEntity<List<Company>> response = null;
		try {
			List<Company> list = service.getAllCompanies();
			response = ResponseEntity.ok(list);
			log.info("fetch all method is success.");
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		log.info("about to leave Fetch all method");
		return response;
	}

	// 3. Fetch one Company
	@GetMapping("/fetch/{id}")
	public ResponseEntity<Company> getCompany(@PathVariable Long id) {
		log.info("Entered into fetch one method.");
		ResponseEntity<Company> response = null;
		try {
			Company com = service.getOneCompany(id);
			response = ResponseEntity.ok(com);
			log.info("fetch one method is success.");
		} catch (CompanyNotFoundException e) {
//			e.printStackTrace();
			log.error(e.getMessage());
			throw e;
		}
		log.info("about to leave Fetch one method");
		return response;
	}

}
